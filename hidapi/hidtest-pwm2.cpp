#include <stdio.h>
#include <wchar.h>
#include <string.h>
#include <stdlib.h>
#include "hidapi.h"

// Defines

/*
#define MAX_OUT_SIZE 64
#define MAX_IN_SIZE 64
*/

#define MAX_OUT_SIZE 16
#define MAX_IN_SIZE 16

#define USBHID_OUT_DATA_ID 0x01
#define USBHID_IN_DATA_ID  0x02

// USB DAQ Packets
#define USB_DAQ_OP_OFFSET 0
#define USB_OUT_DAC_INDEX 2

#define DAC_DEFAULT_VALUE 0x4FF
#define PWM_DEFAULT_VALUE 0x000

typedef enum _USB_APP_CODE {
	APP_CODE_CTRL = 0x00,
	APP_CODE_DAC,
	APP_CODE_PWM,
	APP_CODE_ADC,
} USB_APP_CODE;

typedef enum _USB_GENERAL_CTRL_BITMASK {
	GENERAL_CTRL_DAC_ENABLE = 0x01,
	GENERAL_CTRL_PWM_ENABLE = 0x02,
	GENERAL_CTRL_ADC_ENABLE = 0x04,
} USB_GENERAL_CTRL_BITMASK;

typedef enum _CTRL_CONTROL {
	GEN_CTRL = 0x00,
	CTRL_DAC_CTRL0,
	CTRL_DAC_CTRL1,
	ADC_CTRL,
	ADC_CLK_FREQ,
	ADC_REF,
	PWM_CTRL,
	PWM_FREQ,
} CTRL_CONTROL;

typedef enum _DAQ_DAC_CHANNELS {
	DAC_CHAN_0,
	DAC_CHAN_1,
	DAC_CHAN_2,
	DAC_CHAN_3,
	DAC_CHAN_4,
	DAC_CHAN_5,
	DAC_CHAN_6,
	DAC_CHAN_7,
} DAQ_DAC_CHANNELS;

// Headers needed for sleeping.
#ifdef _WIN32
	#include <windows.h>
#else
	#include <unistd.h>
#endif

void short_sleep(int sleep_msec) {
	#ifdef WIN32
			Sleep(sleep_msec);
	#else
			usleep(sleep_msec * 1000);
	#endif
}

int usbhid_write_packet(hid_device *handle, unsigned char *buffer, int length)
{
	int res;
	int i;

	res = hid_write(handle, buffer, length);
	if (res < 0) {
		printf("Unable to hid_write()\n");
		printf("Error: %ls\n", hid_error(handle));

		return -1;
	}

	for (i = 0; i < length; i++) {
		printf("%02X:", *(buffer + i));
	}
	printf("\n");

	return 0;
}

int usbhid_buddy_init(hid_device *handle)
{
	int res;
	unsigned char out_buf[MAX_OUT_SIZE];

	memset(&out_buf[0], 0x00, MAX_OUT_SIZE);

	// general control options
	out_buf[0] = USBHID_OUT_DATA_ID;
	out_buf[1] = APP_CODE_CTRL;
	out_buf[2] = GEN_CTRL;

	// enable ADC, enable DAC
	out_buf[3] = GENERAL_CTRL_DAC_ENABLE | GENERAL_CTRL_ADC_ENABLE;
	if (usbhid_write_packet(handle, &out_buf[0], MAX_OUT_SIZE) == -1)
		return -1;

	short_sleep(100);

	// DAC
	out_buf[2] = CTRL_DAC_CTRL0;

	// set TLV5630 CTRL0 DAC setting
	out_buf[3] = 0x00;
	if (usbhid_write_packet(handle, &out_buf[0], MAX_OUT_SIZE) == -1)
		return -1;

	short_sleep(100);

	out_buf[2] = CTRL_DAC_CTRL1;
	
	// set TLV5630 CTRL1 DAC setting
	out_buf[3] = 0x00;
	if (usbhid_write_packet(handle, &out_buf[0], MAX_OUT_SIZE) == -1)
		return -1;

	short_sleep(100);

	// PWM

	// ADC

	return 0;
}

int main(int argc, char* argv[])
{
	int res;
	unsigned char buf[256];
	#define MAX_STR 255
	wchar_t wstr[MAX_STR];
	hid_device *handle;
	int i;
	unsigned short k;

	unsigned char out_buf[MAX_OUT_SIZE];
	unsigned char in_buf[MAX_IN_SIZE];
	unsigned char out_index;
	unsigned short next_value;

#ifdef WIN32
	UNREFERENCED_PARAMETER(argc);
	UNREFERENCED_PARAMETER(argv);
#endif

	struct hid_device_info *devs, *cur_dev;
	
	if (hid_init())
		return -1;

	devs = hid_enumerate(0x0, 0x0);
	cur_dev = devs;	
	while (cur_dev) {
		printf("Device Found\n  type: %04hx %04hx\n  path: %s\n  serial_number: %ls", cur_dev->vendor_id, cur_dev->product_id, cur_dev->path, cur_dev->serial_number);
		printf("\n");
		printf("  Manufacturer: %ls\n", cur_dev->manufacturer_string);
		printf("  Product:      %ls\n", cur_dev->product_string);
		printf("  Release:      %hx\n", cur_dev->release_number);
		printf("  Interface:    %d\n",  cur_dev->interface_number);
		printf("\n");
		cur_dev = cur_dev->next;
	}
	hid_free_enumeration(devs);

	// Open the device using the VID, PID,
	// and optionally the Serial number.
	////handle = hid_open(0x4d8, 0x3f, L"12345");
	handle = hid_open(0x10C4, 0x82CD, NULL);
	if (!handle) {
		printf("unable to open device\n");
 		return 1;
	}

	// Read the Manufacturer String
	wstr[0] = 0x0000;
	res = hid_get_manufacturer_string(handle, wstr, MAX_STR);
	if (res < 0)
		printf("Unable to read manufacturer string\n");
	printf("Manufacturer String: %ls\n", wstr);

	// Read the Product String
	wstr[0] = 0x0000;
	res = hid_get_product_string(handle, wstr, MAX_STR);
	if (res < 0)
		printf("Unable to read product string\n");
	printf("Product String: %ls\n", wstr);

	// Read the Serial Number String
	wstr[0] = 0x0000;
	res = hid_get_serial_number_string(handle, wstr, MAX_STR);
	if (res < 0)
		printf("Unable to read serial number string\n");
	printf("Serial Number String: (%d) %ls", wstr[0], wstr);
	printf("\n");

	// Read Indexed String 1
	wstr[0] = 0x0000;
	res = hid_get_indexed_string(handle, 1, wstr, MAX_STR);
	if (res < 0)
		printf("Unable to read indexed string 1\n");
	printf("Indexed String 1: %ls\n", wstr);

	// Set the hid_read() function to be non-blocking.
	hid_set_nonblocking(handle, 1);
	
	memset(buf,0,sizeof(buf));

	// Write configuration and control packets to the USBHID device
	if (usbhid_buddy_init(handle) == -1) {
		printf("Unable to init usbhid device\n");
		return -1;
	}

	// Write sample series of DAC/PWM packets out
	for (k = 0; k <= 4095*32; k++) {
		out_buf[0] = USBHID_OUT_DATA_ID;
		out_buf[1] = APP_CODE_DAC;

		out_index = 2;

		// create a sample DAC or PWM packet
		for (i = 0; i <= DAC_CHAN_7; i++) {
			//out_buf[out_index] = (unsigned char) ((DAC_DEFAULT_VALUE & 0xF00) >> 8);
			//out_buf[out_index + 1] = (unsigned char) DAC_DEFAULT_VALUE;
			
			/*
			*(out_buf + out_index) = DAC_DEFAULT_VALUE;
			*(out_buf + out_index + 1) = ((DAC_DEFAULT_VALUE & 0xF00) >> 8);
			*/

			if (i == 0) {
				if ((k % 2) == 0) {
					next_value = 0xFFF;
				}
				else {
					next_value = 0x00;
				}
			}
			else {
				next_value = k;
			}

			*(out_buf + out_index) = next_value;
			*(out_buf + out_index + 1) = ((next_value & 0xF00) >> 8);

			// out_buf[out_index] = PWM_DEFAULT_VALUE
			out_index += USB_OUT_DAC_INDEX;
		}

		if (usbhid_write_packet(handle, &out_buf[0], MAX_OUT_SIZE) == -1)
			return -1;
	
		#ifdef WIN32
			Sleep(1);
		#else
			usleep(1*1000);
		#endif
	}

	/*
	for (k = 0; k <= 10; k++) {
		buf[0] = 0x02;
		buf[1] = 'A';
		buf[2] = 'B';
		buf[3] = 'C';
		buf[4] = 0x00;
		res = hid_write(handle, buf, 60);
		if (res < 0) {
			printf("Unable to write()\n");
			printf("Error: %ls\n", hid_error(handle));
		}

		buf[0] = 0x01;
		res = 0;
		while (res == 0) {
			res = hid_read(handle, buf, 60);
			printf("res = %d\n", res);

			if (res < 0) {
				printf("Unable to read()\n");
			}
			else if (res > 0) {
				//printf("Data read with len: %d:\n", buf[0]);
				//printf("\n");

				// Print out the returned buffer.
				for (i = 1; i < buf[0] + 1; i++) {
					//printf("%c", buf[i]);
					fprintf(stderr, "%c", buf[i]);
				}
					//printf("%02hd ", buf[i]);
				//printf("\n");
			}

			#ifdef WIN32
				Sleep(500);
			#else
				usleep(500*1000);
			#endif
		}
	}
	*/

	hid_close(handle);

	/* Free static HIDAPI objects. */
	hid_exit();

#ifdef WIN32
	system("pause");
#endif

	return 0;
}
