#include <string.h>
#include <stdio.h>
#include "F3xx_ADC.h"
#include "F3xx_USB_action.h"
#include "F3xx_Blink_Control.h"
#include "F3xx_USB0_InterruptServiceRoutine.h"
#include "F3xx_SPI_DAC.h"
#include "c8051f3xx.h"
#include "globals.h"

#define TIMER0_RELOAD_HIGH  0x2F        // Timer0 High register
#define TIMER0_RELOAD_LOW 0xFF-250            // Timer0 Low register

extern unsigned char adc_change;

extern unsigned char xdata OUT_PACKET[];
extern unsigned char xdata IN_PACKET[];

extern unsigned char out_buf[64];
extern unsigned char in_buf[64];

extern unsigned char SPI_Data_Array[];
unsigned short DAC_TLV5630_Data[TLV5630_CHANNEL_COUNT];

unsigned char USB_IN_temp_buffer[IN_DATA_SIZE];
unsigned char USB_OUT_temp_buffer[OUT_DATA_SIZE];

unsigned char flag_usb_out = 0;
unsigned char flag_usb_in = 0;

extern unsigned char daq_state;
extern unsigned char ctrl_output_flag;
extern unsigned char dac_output_flag;
extern unsigned char pwm_output_flag;
extern unsigned char adc_input_flag;

extern long RESULT[];

extern unsigned char idata PIN_MUX_TABLE[];
extern unsigned char AMUX_INPUT;

unsigned char timer0_flag = 0;
unsigned char timer2_flag = 0;
unsigned char timer4_flag = 0;

unsigned char m_adc_ctrl_value = DEFAULT_ADC0CN;
unsigned char m_adc_ctrl_ref = DEFAULT_REF0CN;
unsigned char m_adc_clk_freq = DEFAULT_ADC0CF;

//-----------------------------------------------------------------------------
// Timer0_Init
//-----------------------------------------------------------------------------
//
// Return Value : None
// Parameters   : None
//
// This function configures the Timer0 as a 16-bit timer, interrupt enabled.
// Using the internal osc. at 12MHz with a prescaler of 1:8 and reloading the
// T0 registers with TIMER0_RELOAD_HIGH/LOW it will interrupt and then toggle
// the LED.
//
// Note: The Timer0 uses a 1:48 prescaler.  If this setting changes, the
// TIMER_PRESCALER constant must also be changed.
//  fCLK = 12e6/48 = 250 kHz, tCLK = 4 usec
//  want 100 usec ADC so 100/4 = 25 count
//  TIMER0_RELOAD_HIGH = 0xFF;
//  TIMER0_RELOAD_LOW  = 0xFF - 25;
//-----------------------------------------------------------------------------
void Timer0_Init(void)
{
   TH0 = TIMER0_RELOAD_HIGH;           // Init Timer0 High register
   TL0 = TIMER0_RELOAD_LOW ;           // Init Timer0 Low register
   TMOD = 0x01;                        // Timer0 in 16-bit mode
   CKCON = 0x02;                       // Timer0 uses a 1:48 prescaler
   ET0 = 1;                            // Timer0 interrupt enabled
   TCON = 0x10;                        // Timer0 ON
}

//-----------------------------------------------------------------------------
// Timer2_Init
//-----------------------------------------------------------------------------
//
// Return Value : None
// Parameters   : None
//
// Configure Timer2 to 16-bit auto-reload and generate an interrupt at 10 us
// intervals.  Timer2 overflows automatically triggers ADC0 conversion.
//
//-----------------------------------------------------------------------------
void Timer2_Init (void)
{
   TMR2CN = 0x00;                      // Stop Timer2; Clear TF2;
                                       // use SYSCLK as timebase, 16-bit
                                       // auto-reload
   CKCON |= 0x10;                      // Select SYSCLK for timer 2 source
   TMR2RL = 65535 - (SYSCLK / 1000);   // Init reload value for 10 us
   TMR2 = 0xffff;                      // Set to reload immediately
   ET2 = 1;                            // Enable Timer2 interrupts
   TR2 = 1;                            // Start Timer2
}

//-----------------------------------------------------------------------------
// Timer4_Init
//-----------------------------------------------------------------------------
//
// Return Value : None
// Parameters   : None
//
// Configure Timer2 to 16-bit auto-reload and generate an interrupt at 10 us
// intervals.  Timer2 overflows automatically triggers ADC0 conversion.
//
//-----------------------------------------------------------------------------
void Timer4_Init (void)
{
   TMR4CN = 0x00;                      // Stop Timer4; Clear TF4;
                                       // use SYSCLK/12 as timebase, 16-bit
                                       // auto-reload
   CKCON1 |= 0x01;                     // Select SYSCLK for timer 2 source
   TMR4RL = 65535 - (SYSCLK / 10000);  // Init reload value for 10 us
   //TMR4RL = 0xD0;
   //TMR4RH = 0xFF;
    
   TMR4 = 0xffff;                      // Set to reload immediately
   //ET4 = 1;                            // Enable Timer2 interrupts
   EIE2 |= 0x10;
    
   //TR4 = 1;                            // Start Timer2
   TMR4CN |= 0x04;
   //TR4 = 1;
}

//-----------------------------------------------------------------------------
// Interrupt Service Routines
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// Timer0_ISR
//-----------------------------------------------------------------------------
//
// Here we process the Timer0 interrupt and toggle the LED
//
//-----------------------------------------------------------------------------
void Timer0_ISR (void) interrupt 1
{
   TH0 = TIMER0_RELOAD_HIGH;           // Reinit Timer0 High register
   TL0 = TIMER0_RELOAD_LOW;            // Reinit Timer0 Low register
    
   timer0_flag = 1;
   
   /*
   printf("Timer0_ISR\r\n");
   */
}

//-----------------------------------------------------------------------------
// Timer2_ISR
//-----------------------------------------------------------------------------
//
// This routine changes to the next Analog MUX input whenever Timer2 overflows
// for the next ADC sample.  This allows the ADC to begin setting on the new
// input while converting the old input.
//
void Timer2_ISR (void) interrupt 5
{
   TF2H = 0;
   timer2_flag = 0; 

	 /*
   if (daq_state == GENERAL_CTRL_ADC_ENABLE) {
       if (AMUX_INPUT == (ANALOG_INPUTS - 1)) {
           AMX0P = PIN_MUX_TABLE[0];
       }
       else {
           AMX0P = PIN_MUX_TABLE[AMUX_INPUT + 1];
       }
   }
	 */

	     if (AMUX_INPUT == (ANALOG_INPUTS - 1)) {
           AMX0P = PIN_MUX_TABLE[0];
       }
       else {
           AMX0P = PIN_MUX_TABLE[AMUX_INPUT + 1];
       }
}

//-----------------------------------------------------------------------------
// Timer4_ISR
//-----------------------------------------------------------------------------
//
// This routine changes to the next Analog MUX input whenever Timer2 overflows
// for the next ADC sample.  This allows the ADC to begin setting on the new
// input while converting the old input.
//
void Timer4_ISR (void) interrupt 19
{
   TMR4CN &= ~0x80;                           // Clear Timer4 interrupt flag
    
   timer4_flag = 1;
}

//-----------------------------------------------------------------------------
// DAQ_Print_Status
//-----------------------------------------------------------------------------
//
// Return Value : None
// Parameters   : None
//
// Print status report of DAQ value set.  Prints the DAC or PWM value received
// through the USB OUT report.  Prints the ADC value being sent in the USB IN
// report.  
// 
//-----------------------------------------------------------------------------
void DAQ_Print_Status()
{
    int i;
    
    printf("dac_output_flag = %bd\r\n", dac_output_flag);
    printf("\r\n");
    
    for (i = 0; i < TLV5630_CHANNEL_COUNT; i++) {
        printf("DAC_TLV5630_Data[%d] = %04x\r\n", i, DAC_TLV5630_Data[i]);
    }
    
    printf("pwm_output_flag = %bd\r\n", pwm_output_flag);
    printf("\r\n");
    printf("adc_input_flag = %bd\r\n", adc_input_flag);
    printf("\r\n");
    
    for (i = 0; i < ANALOG_INPUTS; i++) {
        printf("RESULT[%d] = %04x\r\n", i, RESULT[i]);
    }
}

//-----------------------------------------------------------------------------
// Process_DAC_Packet
//-----------------------------------------------------------------------------
//
// Return Value : None
// Parameters   : None
//
// Process USB IN packet for DAC values.  The format of DAC values
// is specified in the datasheet.  
//
//-----------------------------------------------------------------------------
void Process_DAC_Packet()
{
    unsigned char *usb_packet_ptr = &IN_PACKET[1];
    unsigned char packet_byte_01, packet_byte_02;
    unsigned short packet_value;
    int i;
    
    /*
    for (i = 0; i < OUT_DATA_SIZE; i++) {
        printf("%02bX:", out_buf[i]);
    }
    printf("\r\n");
    */
    
    usb_packet_ptr = &out_buf[2];
    
    //printf("Process_DAC_Packet\r\n");
    
    for (i = REG_DAC_A; i <= REG_DAC_H; i++) {
        packet_byte_01 = *(usb_packet_ptr);
        packet_byte_02 = *(usb_packet_ptr + 1);

        /*
        printf("packet_byte_01 = %02bx\r\n", packet_byte_01);
        printf("packet_byte_02 = %02bx\r\n", packet_byte_02);
        */
        
        packet_value = (packet_byte_02 << 8) | packet_byte_01;

        //printf("i = %d, packet_value = %04hX\r\n", i, packet_value);
        DAC_TLV5630_Data[i-REG_DAC_A] = packet_value;
        
        usb_packet_ptr += 2; // advance 2 byte for next word
    }
}

//-----------------------------------------------------------------------------
// DAQ_USB_Update
//-----------------------------------------------------------------------------
//
// Return Value : None
// Parameters   : None
//
// Process USB IN/OUT packets.  Copies the OUT buffer with specified
// DAC or PWM values into a local buffer.  Copies the ADC values to the
// IN buffer.  Parses the raw packet format into a structure of values
// for DAC or PWM values.  
//
//-----------------------------------------------------------------------------
void DAQ_USB_Update()
{
    // copy USB IN/OUT packets to/from local buffer
    // and print status report
    DAQ_Process();
    //DAQ_Print_Status();
}

//-----------------------------------------------------------------------------
// DAQ_Process
//-----------------------------------------------------------------------------
//
// Return Value : None
// Parameters   : None
//
// Process to run DAC, PWM, and ADC components. If the control register 
// value has for peripheral changes then update the peripheral setting.  
// Looks at the control register settings for PWM and ADC and selects 
// which one to use.  Checks the USB IN packet for incoming ADC data of 
// width 10 bits and and also looks at the USB OUT packet for incoming 
// DAC or PWM data.  
//
//-----------------------------------------------------------------------------
void DAQ_Process()
{
    int i;
    unsigned short dac_channel_value;
    
    /*
    printf("DAQ_Process invoked\r\n");
    printf("dac_output_flag = %bd\r\n", dac_output_flag);
    printf("daq_state = %02bX\r\n", daq_state);
    */
    
    //printf("-> DAQ_Process invoked\r\n");
    
    // DAC handle
    //if (dac_output_flag && (daq_state == GENERAL_CTRL_DAC_ENABLE)) {
    if ((dac_output_flag) && (daq_state == GENERAL_CTRL_DAC_ENABLE)) {
        dac_output_flag = 0;
        
        // printf("[**] Sending SPI DAC packets = %02bx\r\n", daq_state);
        
        // USBHID OUT
        Process_DAC_Packet();
        
        for (i = REG_DAC_A; i <= REG_DAC_H; i++) {
            // channel
            SPI_Data_Array[0] = (i << 4);
            //printf("initial SPI_Data_Array[0] = %02bX\r\n", SPI_Data_Array[0]);
            
            // value
            // SPI_Data_Array[1] = hi-word
            // SPI_Data_Array[0] = lo-word
            dac_channel_value = DAC_TLV5630_Data[i - REG_DAC_A];
            
            //printf("dac_channel_value = %hd\r\n", dac_channel_value);
            
            SPI_Data_Array[0] |= ((dac_channel_value & 0xF00) >> 8);
            SPI_Data_Array[1] = dac_channel_value & 0xFF;
       
            #ifdef DEBUG
                printf("SPI_Data_Array[0] = %02bX\r\n", SPI_Data_Array[0]);
                printf("SPI_Data_Array[1] = %02bX\r\n", SPI_Data_Array[1]);
            #endif
            
            // Send the array to the slave
            SPI_Array_Write();

            // Wait until the Write transfer has // finished  
            while (!NSSMD0);      
        }
    }
    else if (daq_state == GENERAL_CTRL_ADC_ENABLE) {
       adc_change = 1;
    }
    
    /*
    // PWM handle
    if (pwm_output_flag && (daq_state == GENERAL_CTRL_PWM_ENABLE)) {
        pwm_output_flag = 0;
    }
    */
}

void ADC_Control_Set(unsigned char ctrl_value)
{
    //ADC0CN = ctrl_value;
    m_adc_ctrl_value = ctrl_value;
}

void ADC_Clock_Set(unsigned char ctrl_value)
{
    //REF0CN = ctrl_value;
    m_adc_clk_freq = ctrl_value;
}

void ADC_Reference_Set(unsigned char ctrl_value)
{
    //ADC0CF = ctrl_value;
    m_adc_ctrl_ref = ctrl_value;
    
    ADC0_Init();
    flag_usb_in = 1;
}

int DAQ_CTRL_Process()
{
    unsigned char ctrl_value;
    unsigned char ctrl_type;
    
    ctrl_value = out_buf[USBHID_TYPE_VALUE];
    ctrl_type = out_buf[USBHID_APP_INDIC_OFFSET];
    
    printf("DAQ_CTRL_Process invoked\r\n");
    printf("%02bX:%02bX:%02bX:%02bX\r\n", out_buf[0], out_buf[1], out_buf[2], out_buf[3]);
    printf("ctrl_value = %02bX\r\n", ctrl_value);
    printf("ctrl_type = %02bX\r\n", ctrl_type);
    
    // check for which CTRL setting to modify
    switch (ctrl_type) 
    {
        case GEN_CTRL:
            // DAC and PWM out_state
            // DAC takes precedence over PWM
            if (out_buf[USBHID_TYPE_VALUE] == GENERAL_CTRL_DAC_ENABLE) {
                daq_state = GENERAL_CTRL_DAC_ENABLE;
                // printf("daq_state <- GENERAL_CTRL_DAC_ENABLE = %02bx\r\n", daq_state);
                //daq_state |= ~GENERAL_CTRL_PWM_ENABLE;
            }
            /*
            else if (out_buf[USBHID_TYPE_VALUE] == GENERAL_CTRL_PWM_ENABLE) {
                daq_state = GENERAL_CTRL_PWM_ENABLE;
                //daq_state |= ~GENERAL_CTRL_DAC_ENABLE;
            }
            */
            else if (out_buf[USBHID_TYPE_VALUE]== GENERAL_CTRL_ADC_ENABLE) {
                daq_state = GENERAL_CTRL_ADC_ENABLE;
                flag_usb_in = 1;
            }
            break;
        
        case CTRL_DAC_CTRL0:
            printf("CTRL_DAC_CTRL0 set\r\n");
            TLV5630_Control_Set((REG_CTRL0 << 4), ctrl_value);
            break;
        
        case CTRL_DAC_CTRL1:
            printf("CTRL_DAC_CTRL1 set\r\n");
            TLV5630_Control_Set((REG_CTRL1 << 4), ctrl_value);
            break;
        
        case ADC_CTRL:
            printf("ADC_CTRL\r\n");
            //ADC_Control_Set(ctrl_value);
            break;
        
        case ADC_CLK_FREQ:
            printf("ADC_CLK_FREQ\r\n");
            //ADC_Clock_Set(ctrl_value);
            break;
        
        case ADC_REF:
            printf("ADC_REF\r\n");
            //ADC_Reference_Set(ctrl_value);
            break;

        default:
            return -1;
            break;
    }
    
    printf("daq_state = %02bx\r\n", daq_state);
    
    return 0;
}

//-----------------------------------------------------------------------------
// Process_USB_Packets
//-----------------------------------------------------------------------------
//
// Return Value : None
// Parameters   : None
//
// Copies USB IN and OUT USB buffers to local buffer arrays that
// can be gated and are not written over by a USB generated interrupt.
//
//-----------------------------------------------------------------------------
void Process_USB_Packets()
{
	  static unsigned char proc_usb_cnt = 0;
	
    //IN_PACKET[0] = IN_DATA;
    //SendPacket(IN_DATA);
    
    /*
    // USB IN packet
    // Get ADC input value
    // memcpy( , &in_buf[1], IN_DATA_SIZE-1); 
    */
    
    // USB IN packet
    // if previous USB_APP_CODE set current mode to ADC then
    // flag_usb_in will be set
    if (flag_usb_in) { 
        IN_PACKET[0] = IN_DATA;
        IN_PACKET[1] = 0x04;
        IN_PACKET[2] = 0x05;
				IN_PACKET[3] = proc_usb_cnt;
			
				memcpy(&IN_PACKET[4], &RESULT[0], ANALOG_INPUTS * 2);
			
        SendPacket(IN_DATA);    
				proc_usb_cnt++;
		}
    //usb_packet_cnt = (usb_packet_cnt + 1) % 255;
    //}

    // USB OUT packet
    // DAC or PWM value arrived
    if (flag_usb_out) {
        flag_usb_out = 0;
        
        #ifdef DEBUG
            printf("flag_usb_out = 1, USB OUT packet rcv\r\n");
        
            for (i = 0; i < OUT_DATA_SIZE; i++) {
                printf("%02bX:", out_buf[i]);
            }
            printf("\r\n");
        #endif
        
        // check for CTRL packet
        if (out_buf[USBHID_APP_CODE_OFFSET] == USBHID_ACTION_CTRL) {
            printf("ctrl_output_flag = 1 (CTRL output detected)\r\n");
            DAQ_CTRL_Process();
        }
        else if (out_buf[1] == USBHID_ACTION_DAC) {
            #ifdef DEBUG
                printf("dac_output_flag = 1 (DAC output detected)\r\n");
            #endif
            
            dac_output_flag = 1;
        }
        // check if PWM or DAC
        /*
        if (out_buf[1] == USBHID_ACTION_PWM) {
            printf("pwm_output_flag = 1 (PWM output detected)\r\n");
            pwm_output_flag = 1;
        }
        */
    }
}
