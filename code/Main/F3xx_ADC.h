#ifndef  _F3xx_ADC
#define  _F3xx_ADC

void ADC0_Init(void);

void ADC0_Set_Ctrl(unsigned char control_value);
void ADC0_Set_Frequency(unsigned char control_value);
void ADC0_Set_Reference(unsigned char control_value);

#endif 