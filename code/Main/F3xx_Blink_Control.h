//-----------------------------------------------------------------------------
// F3xx_Blink_Control.h
//-----------------------------------------------------------------------------
// Copyright 2010 Silicon Laboratories, Inc.
// http://www.silabs.com
//
// Program Description:
//
// This file includes all of the Report IDs and variables needed by
// USB_ReportHandler.c to process input and output reports,
// as well as initialization routine prototypes.
//
//
// How To Test:    See Readme.txt
//
//
// FID:            3XX000010
// Target:         C8051F3xx
// Tool chain:     Keil / Raisonance
//                 Silicon Laboratories IDE version 2.6
// Command Line:   See Readme.txt
// Project Name:   F3xx_BlinkyExample
//
// Release 1.2 (ES)
//    -Added support for Raisonance
//    -No change to this file
//    -02 APR 2010
// Release 1.1
//    -Added feature reports for dimming controls
//    -Added PCA dimmer functionality
//    -16 NOV 2006
// Release 1.0
//    -Initial Revision (PD)
//    -07 DEC 2005
//

#ifndef  _BLINK_C_H_
#define  _BLINK_C_H_

extern unsigned char xdata BLINK_SELECTOR;

extern unsigned int xdata BLINK_RATE;
extern unsigned char xdata BLINK_ENABLE;
extern unsigned char xdata BLINK_SELECTORUPDATE;
extern unsigned char xdata BLINK_LED1ACTIVE;
extern unsigned char xdata BLINK_LED2ACTIVE;
extern unsigned char xdata BLINK_DIMMER;
extern unsigned char xdata BLINK_DIMMER_SUCCESS;

extern unsigned char xdata OUT_PACKET[];
extern unsigned char xdata IN_PACKET[];

void System_Init(void);
void Usb_Init(void);


// ----------------------------------------------------------------------------
// Report IDs
// ----------------------------------------------------------------------------
#define OUT_DATA 0x01
#define IN_DATA 0x02

// ----------------------------------------------------------------------------
// Report Sizes (in bytes)
// ----------------------------------------------------------------------------

/*
#define OUT_DATA_SIZE 64
#define IN_DATA_SIZE 63
*/

#define OUT_DATA_SIZE 20
#define IN_DATA_SIZE 20

#endif