#ifndef  _USBUART
#define  _USBUART

#define UART0_BUFFERSIZE 256

//void Timer0_wait(int ms);
void UART0_Init (void);
void UART0_Interrupt(void);
void UART0_print_raw(char *str, int length);
void UART0_print(char *str);
void UART0_flush_rx();

#endif 