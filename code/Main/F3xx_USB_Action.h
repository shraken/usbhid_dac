#ifndef  _USB_ACTION_H_
#define  _USB_ACTION_

#define DEFAULT_ADC0CN 0x02
#define DEFAULT_REF0CN 0x03
#define DEFAULT_ADC0CF (((SYSCLK/3000000)-1)<<3)

#define USBHID_TYPE_OFFSET 0       // USBHID (IN/OUT)
#define USBHID_APP_CODE_OFFSET 1   // APP (Application)
#define USBHID_APP_INDIC_OFFSET 2  // CTRL (Control) Type
#define USBHID_TYPE_VALUE 3        // CTRL (Control) Value

typedef enum _USB_GENERAL_CTRL_BITMASK {
  GENERAL_CTRL_NONE = 0x00,
	GENERAL_CTRL_DAC_ENABLE,
	GENERAL_CTRL_PWM_ENABLE,
	GENERAL_CTRL_ADC_ENABLE
} USB_GENERAL_CTRL_BITMASK;

typedef enum _CTRL_CONTROL {
	GEN_CTRL = 0x00,
	CTRL_DAC_CTRL0,
	CTRL_DAC_CTRL1,
	ADC_CTRL,
	ADC_CLK_FREQ,
	ADC_REF,
	PWM_CTRL,
	PWM_FREQ,
} CTRL_CONTROL;

typedef enum _USBHID_ACTION_CODE {
    USBHID_ACTION_CTRL = 0x00,
    USBHID_ACTION_DAC,
    USBHID_ACTION_PWM,
    USBHID_ACTION_ADC,
} USBHID_ACTION_CODE;

void Timer0_Init(void);
void Timer2_Init (void);
void Timer4_Init (void);
void DAQ_Print_Status();
void Process_DAC_Packet();
void DAQ_USB_Update();
void DAQ_Process();
int DAQ_CTRL_Process();
void Process_USB_Packets();

#endif