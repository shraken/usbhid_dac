//-----------------------------------------------------------------------------
// F3xx_USB_Main.c
//-----------------------------------------------------------------------------
// Copyright 2010 Silicon Laboratories, Inc.
// http://www.silabs.com
//
// Program Description:
//
// This application will communicate with a PC across the USB interface.
// The device will appear to be a mouse, and will manipulate the cursor
// on screen.
//
// How To Test:    See Readme.txt
//
//
// FID:            3XX000006
// Target:         C8051F32x/C8051F340
// Tool chain:     Keil / Raisonance
//                 Silicon Laboratories IDE version 2.6
// Command Line:   See Readme.txt
// Project Name:   F3xx_BlinkyExample
//
// Release 1.2 (ES)
//    -Added support for Raisonance
//    -No change to this file
//    -02 APR 2010
// Release 1.1
//    -Added feature reports for dimming controls
//    -Added PCA dimmer functionality
//    -16 NOV 2006
// Release 1.0
//    -Initial Revision (PD)
//    -07 DEC 2005
//
//-----------------------------------------------------------------------------
// Header Files
//-----------------------------------------------------------------------------

#include "c8051f3xx.h"
#include "F3xx_USB_Action.h"
#include "F3xx_USB0_Register.h"
#include "F3xx_Blink_Control.h"
#include "F3xx_USB0_InterruptServiceRoutine.h"
#include "F3xx_USB0_Descriptor.h"
#include "F3xx_USBUART.h"
#include "F3xx_SPI_DAC.h"
#include "F3xx_ADC.h"
#include "globals.h"
#include <stdio.h>
#include <string.h>

//-----------------------------------------------------------------------------
// Definitions
//-----------------------------------------------------------------------------

#define CEX0_FREQUENCY  50000           // Frequency to output on CEX0 (Hz)
#define CEX1_FREQUENCY  100000          // Frequency to output on CEX0 (Hz)
#define CEX2_FREQUENCY  150000          // Frequency to output on CEX0 (Hz)
#define CEX3_FREQUENCY  200000          // Frequency to output on CEX0 (Hz)
#define CEX4_FREQUENCY  250000          // Frequency to output on CEX0 (Hz)

#define LED_FREQUENCY 5                // Frequency to blink LED at in Hz
#define T0_CLOCKS 120                  // Use 120 clocks per T0 Overflow

// SYSCLK cycles per interrupt
#define PCA_TIMEOUT ((SYSCLK/T0_CLOCKS)/LED_FREQUENCY/2)

#define BAUDRATE           115200           // Baud rate of UART in bps

#define TIMER_PRESCALER            48  // Based on Timer CKCON settings

#define LED_TOGGLE_RATE            50  // LED toggle rate in milliseconds
                                       // if LED_TOGGLE_RATE = 1, the LED will
                                       // be on for 1 millisecond and off for
                                       // 1 millisecond

unsigned int CEX0_Compare_Value;       // Holds current PCA compare value
unsigned int CEX1_Compare_Value;       // Holds current PCA compare value
unsigned int CEX2_Compare_Value;       // Holds current PCA compare value
unsigned int CEX3_Compare_Value;       // Holds current PCA compare value
unsigned int CEX4_Compare_Value;       // Holds current PCA compare value

// Globals
unsigned char adc_change;

unsigned char adc_channel_values[ADC_CHANNEL_COUNT];
unsigned char pwm_channel_values[PWM_CHANNEL_COUNT];
unsigned char dac_channel_values[DAC_CHANNEL_COUNT];

unsigned int glbl_cnt;

extern unsigned char ctrl_output_flag;
extern unsigned char dac_output_flag;
extern unsigned char pwm_output_flag;
extern unsigned char adc_input_flag;

// SPI
extern unsigned char SPI_Data;
extern unsigned char SPI_Data_Array[];
extern bit Error_Flag;
extern unsigned char Command;

extern unsigned char SPI_Data_Array[];

// USB
extern unsigned char in_buf[];
extern unsigned char out_buf[];

extern unsigned char flag_usb_out;
extern unsigned char flag_usb_in;

extern unsigned char timer0_flag;
extern unsigned char timer2_flag;
extern unsigned char timer4_flag;

extern long adc_isr_counter;

void Oscillator_Init (void)
{
   OSCICN = 0x83;                      // Configure internal oscillator for
                                       // its highest frequency
   RSTSRC = 0x04;                      // Enable missing clock detector
}

//-----------------------------------------------------------------------------
// UART0_Init
//-----------------------------------------------------------------------------
//
// Return Value : None
// Parameters   : None
//
// Configure the UART0 using Timer1, for <BAUDRATE> and 8-N-1.
//-----------------------------------------------------------------------------

void UART0_Init (void)
{
   SCON0 = 0x10;                       // SCON0: 8-bit variable bit rate
                                       //        level of STOP bit is ignored
                                       //        RX enabled
                                       //        ninth bits are zeros
                                       //        clear RI0 and TI0 bits
   if (SYSCLK/BAUDRATE/2/256 < 1) {
      TH1 = -(SYSCLK/BAUDRATE/2);
      CKCON &= ~0x0B;                  // T1M = 1; SCA1:0 = xx
      CKCON |=  0x08;
   } else if (SYSCLK/BAUDRATE/2/256 < 4) {
      TH1 = -(SYSCLK/BAUDRATE/2/4);
      CKCON &= ~0x0B;                  // T1M = 0; SCA1:0 = 01                  
      CKCON |=  0x01;
   } else if (SYSCLK/BAUDRATE/2/256 < 12) {
      TH1 = -(SYSCLK/BAUDRATE/2/12);
      CKCON &= ~0x0B;                  // T1M = 0; SCA1:0 = 00
   } else {
      TH1 = -(SYSCLK/BAUDRATE/2/48);
      CKCON &= ~0x0B;                  // T1M = 0; SCA1:0 = 10
      CKCON |=  0x02;
   }

   TL1 = TH1;                          // Init Timer1
   TMOD &= ~0xf0;                      // TMOD: timer 1 in 8-bit autoreload
   TMOD |=  0x20;                       
   TR1 = 1;                            // START Timer1
   TI0 = 1;                            // Indicate TX0 ready
}

/*
void Timer0_wait(int ms)
{
    TMOD |= 0x01;                       // Timer0 in 16-bit mode
    CKCON |= 0xF9;                      // Timer0 uses a 1:4 prescaler

    while(ms)
    {
        TH0 = TIMER0_RELOAD_HIGH;       // Init Timer0 High register
        TL0 = TIMER0_RELOAD_LOW;        // Init Timer0 Low register
        
        TR0 = 1;                        // Enable the Timer0
        TF0 = 0;                        // Clear flag to initialize
        while(!TF0);                    // Wait until timer overflows
        TR0 = 0;                        // Timer0 OFF
    
        ms--;                           // Decrement ms
    }

    TR0 = 0;                            // Timer0 OFF
}
*/

//-----------------------------------------------------------------------------
// PCA0_Init
//-----------------------------------------------------------------------------
//
// Return Value : None
// Parameters   : None
//
// This function configures the PCA time base, and sets up frequency output
// mode for Module 0 (CEX0 pin).
//
// The frequency generated at the CEX0 pin is equal to CEX0_FREQUENCY Hz,
// which is defined in the "Global Constants" section at the beginning of the
// file.
//
// The PCA time base in this example is configured to use SYSCLK / 12.
// The frequency range that can be generated using this example is ~2 kHz to
// ~500 kHz when the processor clock is 12 MHz.  Using different PCA clock
// sources or a different processor clock will generate different frequency
// ranges.
//
//    -------------------------------------------------------------------------
//    How "Frequency Output Mode" Works:
//
//       The PCA's Frequency Output Mode works by toggling an output pin every
//    "N" PCA clock cycles.  The value of "N" should be loaded into the PCA0CPH
//    register for the module being used (in this case, module 0).  Whenever
//    the register PCA0L (PCA counter low byte) and the module's PCA0CPL value
//    are equivalent, two things happen:
//
//    1) The port pin associated with the PCA module toggles logic state
//    2) The value stored in PCA0CPH is added to PCA0CPL.
//
//    Using this mode, a square wave is produced at the output port pin of the
//    PCA module. The speed of the waveform can be changed by changing the
//    value of the PCA0CPH register.
//    -------------------------------------------------------------------------
//
//-----------------------------------------------------------------------------
void PCA0_Init (void)
{
   // Configure PCA time base; overflow interrupt disabled
   PCA0CN = 0x00;                      // Stop counter; clear all flags
   PCA0MD = 0x00;                      // Use SYSCLK/12 as time base

   PCA0CPM0 = 0x46;                    // Module 0 = Frequency Output mode
   PCA0CPM1 = 0x46;                    
   PCA0CPM2 = 0x46;                    
   PCA0CPM3 = 0x46;                    
   PCA0CPM4 = 0x46;                    
    
   // Configure frequency for CEX0
   // PCA0CPH0 = (SYSCLK/12)/(2*CEX0_FREQUENCY), where:
   // SYSCLK/12 = PCA time base
   // CEX0_FREQUENCY = desired frequency
   PCA0CPH0 = (SYSCLK/12)/(2*CEX0_FREQUENCY);
   PCA0CPH1 = (SYSCLK/12)/(2*CEX1_FREQUENCY);
   PCA0CPH2 = (SYSCLK/12)/(2*CEX2_FREQUENCY);
   PCA0CPH3 = (SYSCLK/12)/(2*CEX3_FREQUENCY);
   PCA0CPH4 = (SYSCLK/12)/(2*CEX4_FREQUENCY);

   // Start PCA counter
   CR = 1;
}

//-----------------------------------------------------------------------------
// Main Routine
//-----------------------------------------------------------------------------
void main(void)
{
    char buf[128];
    int cnt = 0;
    unsigned int main_cnt = 0;
    
    flag_usb_out = 1;
    flag_usb_in = 0;
   
    ctrl_output_flag = 0;
    dac_output_flag = 0;
    pwm_output_flag = 0;
    adc_input_flag = 0;
    
	  Oscillator_Init();
    System_Init();
    Usb_Init();
    SPI0_Init();

    //Timer0_Init();
    Timer2_Init();
    //Timer4_Init();
    
    ADC0_Init();
    UART0_Init();
    printf("UART0_Init passed\r\n");
    
    EA = 1;
    
    TLV5630_DAC_Init();
    printf("TLV5630_DAC_Init passed\r\n");
    
    while (1)
    {
        Process_USB_Packets();
        
        // ADC timer interrupt
        /*
        if (timer0_flag) {
            timer0_flag = 0;
            //printf("timer0_flag, main_cnt = %d\r\n", main_cnt);
            //main_cnt++;
        }
        */
        
        // General DAQ Update interrupt
        if (timer2_flag) {
            timer2_flag  = 0;
            DAQ_USB_Update();
            //printf("timer2_flag, main_cnt = %d\r\n", main_cnt);
            //main_cnt++;
        }

        /*
        if (timer4_flag) {
            timer4_flag  = 0;
            //printf("timer4_flag, main_cnt = %d\r\n", main_cnt);
            main_cnt++;
        }
        */
        
        //Timer0_wait(1);
        glbl_cnt++;
    }
}

