#include <string.h>
#include <stdio.h>
#include "F3xx_ADC.h"
#include "F3xx_USB_action.h"
#include "F3xx_Blink_Control.h"
#include "F3xx_USB0_InterruptServiceRoutine.h"
#include "F3xx_SPI_DAC.h"
#include "c8051f3xx.h"
#include "globals.h"

void ADC0_Init(void);

extern unsigned char daq_state;

extern unsigned char m_adc_ctrl_value;
extern unsigned char m_adc_ctrl_ref;
extern unsigned char m_adc_clk_freq;

static long adc_isr_counter = 0;

//-----------------------------------------------------------------------------
// Global Variables
//-----------------------------------------------------------------------------

long RESULT[ANALOG_INPUTS];            // ADC0 decimated value, one for each
                                       // analog input

// The <PIN_MUX_TABLE> values are the values to be written to the AMX0P
// register to select the P2.<PIN_TABLE> port pins.
// For the 'F380, the AMX0P settings correspond to the following port pins:
//
//    AMX0P      Port Pin
//    0x00         P2.0
//    0x01         P2.1
//    0x02         P2.2
//    0x03         P2.3
//    0x04         P2.5
//    0x05         P2.6
//
// unsigned char idata PIN_TABLE[ANALOG_INPUTS] = {0,1,2,3,5,6};
unsigned char idata PIN_MUX_TABLE[ANALOG_INPUTS] = { 0x00, 
                                                     0x00, 
                                                     0x00, 
                                                     0x00, 
                                                     0x00, 
                                                     0x00, 
                                                     0x00, 
                                                     0x00 };

unsigned char AMUX_INPUT = 0;          // Index of analog MUX inputs

//-----------------------------------------------------------------------------
// ADC0_Init
//-----------------------------------------------------------------------------
//
// Return Value : None
// Parameters   : None
//
// Configures ADC0 to make single-ended analog measurements on Port 2 according
// to the values of <ANALOG_INPUTS> and <PIN_TABLE>.
//
//-----------------------------------------------------------------------------
void ADC0_Init()
{
   AD0EN  = 0;                         // Disable ADC0
   
	 ADC0CN = 0x02;                      // ADC0 disabled, normal tracking,
                                       // conversion triggered on TMR2 overflow
   //ADC0CN = m_adc_ctrl_value;          // ADC0 disabled, normal tracking,
                                       // conversion triggered on TMR2 overflow

	 REF0CN = 0x03;                      // Enable internal VREF
   //REF0CN = m_adc_ctrl_ref;            // Enable internal VREF

   AMX0P = PIN_MUX_TABLE[0];           // ADC0 initial positive input = P2.0
   AMX0N = 0x1F;                       // ADC0 negative input = GND
                                       // i.e., single ended mode

	 ADC0CF = ((SYSCLK/3000000)-1)<<3;   // Set SAR clock to 3MHz
   //ADC0CF = m_adc_clk_freq;            // Set SAR clock to 3MHz

   ADC0CF |= 0x00;                     // Right-justify results

   EIE1 |= 0x08;                       // Enable ADC0 EOC interrupt

   AD0EN = 1;                          // Enable ADC0
}

void ADC0_Set_Ctrl(unsigned char control_value)
{
		ADC0CN = control_value;
}

void ADC0_Set_Frequency(unsigned char control_value)
{
		ADC0CF = control_value;
}

void ADC0_Set_Reference(unsigned char control_value)
{
		REF0CN = control_value;
}

//-----------------------------------------------------------------------------
// Interrupt Service Routines
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// ADC0_ISR
//-----------------------------------------------------------------------------
//
// This ISR averages <INT_DEC> samples for each analog MUX input then prints
// the results to the terminal.  The ISR is called after each ADC conversion,
// which is triggered by Timer2.
//
//-----------------------------------------------------------------------------
void ADC0_ISR (void) interrupt 10
{
   static unsigned int_dec = INT_DEC;  // Integrate/decimate counter
                                       // A new result is posted when
                                       // int_dec is 0

   // Integrate accumulator for the ADC samples from input pins
   static long accumulator[ANALOG_INPUTS] = 0x00000000;

   unsigned char i;                    // Loop counter

   AD0INT = 0;                         // Clear ADC conversion complete
	
	
   accumulator[AMUX_INPUT] += ADC0;    // Read the ADC value and add it to the
                                       // running total

   // Reset sample counter <int_dec> and <AMUX_INPUT> if the final input was
   // just read
   if(AMUX_INPUT == (ANALOG_INPUTS - 1))
   {
      int_dec--;                       // Update decimation counter
                                       // when the last of the analog inputs
                                       // is sampled

      if (int_dec == 0)                // If zero, then post the averaged
      {                                // results
        //printf("int_dec = 0, computing ADC average\r\n"); 
				int_dec = INT_DEC;            // Reset counter

         // Copy each averaged ADC0 value into the RESULT array
         for(i = 0; i < ANALOG_INPUTS; i++)
         {
            // Copy averaged values into RESULT
            RESULT[i] = accumulator[i] / int_dec;

            // Reset accumulators
            accumulator[i] = 0x00000000;
         }
      }

      AMUX_INPUT = 0;                  // Reset input index back to P2.0
   }
   // Otherwise, increment the AMUX channel counter
   else
   {
      AMUX_INPUT++;                    // Step to the next analog mux input
   }
}