#ifndef  _SPIDAC_H_
#define  _SPIDAC_H_

void TLV5630_DAC_Init();
void DAQ_USB_Update();
void DAQ_Process();

extern unsigned char xdata IN_PACKET[];

void TLV5630_Control_Set(unsigned char reg_ctrl, reg_value);
void TLV5630_DAC_Init();
void SPI0_Init();
void SPI_Byte_Write (void);
void SPI_Byte_Read (void);
void SPI_Array_Write (void);
void SPI_Array_Read (void);

//-----------------------------------------------------------------------------
// Global Constants
//-----------------------------------------------------------------------------

#define SPI_CLOCK          1000000      // Maximum SPI clock
                                       // The SPI clock is a maximum of 250 kHz
                                       // when this example is used with
                                       // the SPI0_Slave code example.

#define DAC_CHANNEL_COUNT 8
#define PWM_CHANNEL_COUNT 8
#define ADC_CHANNEL_COUNT 8

#define TLV5630_CHANNEL_COUNT 8        // TLV5630 Number of Channels

#define MAX_BUFFER_SIZE    2           // Maximum buffer Master will send

// Instruction Set
#define  SLAVE_LED_ON      0x01        // Turn the Slave LED on
#define  SLAVE_LED_OFF     0x02        // Turn the Slave LED off
#define  SPI_WRITE         0x04        // Send a byte from the Master to the
                                       // Slave
#define  SPI_READ          0x08        // Send a byte from the Slave to the
                                       // Master
#define  SPI_WRITE_BUFFER  0x10        // Send a series of bytes from the
                                       // Master to the Slave
#define  SPI_READ_BUFFER   0x20        // Send a series of bytes from the Slave
                                       // to the Master
#define  ERROR_OCCURRED    0x40        // Indicator for the Slave to tell the
                                       // Master an error occurred

typedef enum _TLV5630_CTRL0_BITMASK {
    CTRL0_INPUT_MODE,    // IM, 0 = straight binary, 1 = twos complement
    CTRL0_REFERENCE_0,   // R0, 01 = external, 02 = internal 1V, 03 = internal 2V
    CTRL0_REFERENCE_1,   // R1
    CTRL0_DOUT,          // DOUT, 0 = SPI DOUT disable, 1 = enabled
    CTRL0_POWER,         // Full Power Down, 0 = Power Down disabled, 1 = Power Down enabled
} TLV5630_CTRL0_BITMASK;

typedef enum _TLV5630_CTRL1_BITMASK {
    CTRL1_SPEED_DAC_AB,  // Speed AB, 1 = fast, 0 = slow
    CTRL1_SPEED_DAC_CD,  // Speed CD, 1 = fast, 0 = slow
    CTRL1_SPEED_DAC_EF,  // Speed EF, 1 = fast, 0 = slow
    CTRL1_SPEED_DAC_GH,  // Speed GH, 1 = fast, 0 = slow
    CTRL1_POWER_DAC_AB,  // Power Down Channel AB, 0 = disabled, 1 = enabled
    CTRL1_POWER_DAC_CD,  // Power Down Channel CD, 0 = disabled, 1 = enabled
    CTRL1_POWER_DAC_EF,  // Power Down Channel EF, 0 = disabled, 1 = enabled
    CTRL1_POWER_DAC_GH,  // Power Down Channel GH, 0 = disabled, 1 = enabled
} TLV5630_CTRL1_BITMASK;

typedef enum _TLV5630_REGISTERS {
    REG_DAC_A,
    REG_DAC_B,
    REG_DAC_C,
    REG_DAC_D,
    REG_DAC_E,
    REG_DAC_F,
    REG_DAC_G,
    REG_DAC_H,
    REG_CTRL0,
    REG_CTRL1,
    REG_PRESET,
    REG_RESV,
    REG_DAC_A_B_COMP,
    REG_DAC_C_D_COMP,
    REG_DAC_E_F_COMP,
    REG_DAC_G_H_COMP,
} TLV5630_REGISTERS;

typedef enum _TLV5630_CTRL0 {
    CTRL0_IM, // Input Mode, default = 0
    CTRL0_R0, // Reference Select bit 0, default = 0
    CTRL0_R1, // Reference Select bit 1, default = 0
    CTRL0_D0, // DOUT enable, default = 0
    CTRL0_PD, // Full Device power down, default = 0
} TLV5630_CTRL0;

typedef enum _TLV5630_CTRL1 {
    CTRL1_SAB, // Speed DAC A & DAC B, default = 0
    CTRL1_SCD, // Speed DAC C & DAC D, default = 0
    CTRL1_SEF, // Speed DAC E & DAC F, default = 0
    CTRL1_SGH, // Speed DAC G & DAC H, default = 0
    CTRL1_PAB, // Power Down DAC A & DAC B, default = 0
    CTRL1_PCD, // Power Down DAC C & DAC D, default = 0
    CTRL1_PEF, // Power Down DAC E & DAC F, default = 0
    CTRL1_PGH, // Power Down DAC G & DAC H, default = 0
} TLV5630_CTRL1;

void SPI0_Init();
void SPI_Byte_Write (void);
void SPI_Byte_Read (void);
void SPI_Array_Write (void);
void SPI_Array_Read (void);

#endif