
/** @brief This function configures the Timer0 as a 16-bit timer, 
  *         interrupt enabled. Using the internal osc. at 12MHz 
  *         with a prescaler of 1:8 and reloading the T0 registers 
  *         with TIMER0_RELOAD_HIGH/LOW, it will wait for <ms> 
  *         milliseconds.
  *
  *  @param ms number of milliseconds to wait
  *  @return void.
 */

#include <string.h>
#include "F3xx_USBUART.h"
#include "globals.h"
#include "c8051f3xx.h"