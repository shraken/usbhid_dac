#ifndef _GLOBALS_H_
#define _GLOBALS_H_

// #define DEBUG

#define SYSCLK             12000000    // SYSCLK frequency in Hz

#define ANALOG_INPUTS 8                // Number of AIN pins to measure,
                                       // skipping the UART0 pins
#define INT_DEC       2                // Integrate and decimate ratio

#endif